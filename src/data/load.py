import pandas as pd
import os
import sys

os.chdir(os.path.join(os.path.dirname(__file__), "..", ".."))
sys.path.append(os.path.join(os.path.dirname(__file__), "..", ".."))

from src.utils.common_utils import check_kaggle_credential

import logging
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)
logging.basicConfig(format='%(asctime)s | %(levelname)s : %(message)s',
                     level=logging.INFO, stream=sys.stdout)


def download_competition_data():
    '''
        Download the competition data to the data folder
    '''
    kaggle_api = check_kaggle_credential()
    logger.info("Downloading competition data")
    kaggle_api.competitionDownloadFiles("santander-value-prediction-challenge", path = "data/")

def load_competition_data():
    '''
        Import the csv data to Pandas

        :return: home credit risk data
        :rtype: list of DataFrames
    '''
    path = "data/santander-value-prediction-challenge/"
    if not os.path.exists(path + "train.csv.zip"):
        download_competition_data()
    logger.info("Loading competition data")
    df_train = pd.read_csv(path + "train.csv.zip")
    df_test = pd.read_csv(path + "test.csv.zip")
    return df_train, df_test
