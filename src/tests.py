import os
import sys

import json
import great_expectations as ge
import unittest
from data.load import download_competition_data


import logging
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)
logging.basicConfig(format='%(asctime)s | %(levelname)s : %(message)s',
                     level=logging.INFO, stream=sys.stdout)


def test_dataset(file):
    # Load the config file
    with open('src/test/'+ file +'.json') as json_file:
        config = json.load(json_file)
    filename = file +".csv.zip"
    if not os.path.exists(filename):
        download_competition_data()
    df_ = ge.read_csv(
        "data/santander-value-prediction-challenge/" + filename,
        expectations_config=config)
    # Validate dateset with expectations
    return df_.validate()

class TestProject(unittest.TestCase):
    def test_train_dataset(self):
        logger.info("Testing the train dataset")
        results = test_dataset('train')
        logger.info("Results: " + str(results['statistics']))
        self.assertTrue(results['success'])

    def test_test_dataset(self):
        logger.info("Testing the test dataset")
        results = test_dataset('test')
        logger.info("Results: " + str(results['statistics']))
        self.assertTrue(results['success'])

if __name__ == '__main__':
    unittest.main()
