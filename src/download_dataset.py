import os
import pandas as pd

from data.load import download_competition_data

if __name__ == '__main__':
    download_competition_data()
