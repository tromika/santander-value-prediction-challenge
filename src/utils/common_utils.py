import os, json
import shutil
import errno
from os.path import expanduser, isfile

def check_kaggle_credential():
    '''
        Check existence of kaggle credentials inside the container
        Creates the credentials if it's provided via env vars
    '''

    home = expanduser("~")
    kaggle_cred = home + "/.kaggle/kaggle.json"
    if not isfile("kaggle_cred"):
        try:
            kaggle_username = os.environ["KAGGLE_USERNAME"]
            kaggle_key = os.environ["KAGGLE_KEY"]
        except:
            raise KeyError('Please set the kaggle_username and kaggle_key env vars based on your kaggle.json')
        with open(kaggle_cred, 'w') as f:
            json.dump({"username":kaggle_username, "key":kaggle_key}, f)
    import kaggle.api as kaggle_api
    return kaggle_api
