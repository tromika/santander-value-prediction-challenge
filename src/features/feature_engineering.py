import pandas as pd
import os
import sys
import numpy as np

os.chdir(os.path.join(os.path.dirname(__file__), "..", ".."))
sys.path.append(os.path.join(os.path.dirname(__file__), "..", ".."))

import logging
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)
logging.basicConfig(format='%(asctime)s | %(levelname)s : %(message)s',
                     level=logging.INFO, stream=sys.stdout)

def add_statistics(data):
    # This is part of the trick I think, plus lightgbm has a special process for NaNs
    data = data.replace(0, np.nan)

    logger.info("Memory usage: " + str(data.memory_usage(index=True).sum() / 1024**2) + " MB")
    for col in data.columns:
        if data[col].dtype != object:
            if data[col].dtype == float:
                data[col] = data[col].astype(np.float32)
            if data[col].dtype == int:
                data[col] = data[col].astype(np.int32)
    logger.info("Memory usage after compression: "+ str(data.memory_usage(index=True).sum() / 1024**2) + " MB")

    original_features = [f for f in data.columns if f not in ['target', 'ID']]
    data['nb_nans'] = data[original_features].isnull().sum(axis=1)
    # All of the stats will be computed without the 0s
    data['the_median'] = data[original_features].median(axis=1)
    data['the_mean'] = data[original_features].mean(axis=1)
    data['the_sum'] = data[original_features].sum(axis=1)
    data['the_std'] = data[original_features].std(axis=1)
    data['the_kur'] = data[original_features].kurtosis(axis=1)

    return data

def get_features(df_):
    df_ = add_statistics(df_)
    return df_
