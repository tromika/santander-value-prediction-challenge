import os, sys
import pandas as pd
import gc

from data.load import load_competition_data
from features.feature_engineering import get_features

import logging
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)
logging.basicConfig(format='%(asctime)s | %(levelname)s : %(message)s',
                     level=logging.INFO, stream=sys.stdout)

if __name__ == '__main__':
    gc.enable()
    df_train, df_test  = load_competition_data()
    logger.info('Shape of the train dataset: '+ str(df_train.shape))
    logger.info('Shape of the test dataset: '+ str(df_test.shape))
    logger.info('Build features for train...')
    df_train = get_features(df_train)
    logger.info('Build features for test...')
    df_test = get_features(df_test)
    logger.info('OK')
